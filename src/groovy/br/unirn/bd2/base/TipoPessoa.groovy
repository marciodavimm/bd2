package br.unirn.bd2.base
/**
 * Created by marcio on 23/11/14.
 */
enum TipoPessoa {
    PROFESSOR("Professor"), ALUNO("Aluno")

    String descricao

    private TipoPessoa(descricao){
        this.descricao = descricao
    }

    String toString(){descricao}
}