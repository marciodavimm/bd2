# Git Command line #

## Começando do zero ##

### Configurar repositório local ###

```
#!bash

mkdir /path/to/your/project
cd /path/to/your/project
git init
git remote add origin https://marciodavimm@bitbucket.org/marciodavimm/bd2.git
```


### Criar primeiro arquivo, commit, e push ###


```
#!bash

echo "Marcio Maciel" >> contributors.txt
git add contributors.txt
git commit -m 'Initial commit with contributors'
git push -u origin master
```



## Tenho um repositório existente ##

### Caso ja possua repositório local, fazer push dele para o bitbucket. ###


```
#!bash

cd /path/to/my/repo
git remote add origin https://marciodavimm@bitbucket.org/marciodavimm/bd2.git
git push -u origin --all # pushes up the repo and its refs for the first time
git push -u origin --tags # pushes up any tags
```

