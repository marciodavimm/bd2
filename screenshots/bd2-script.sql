﻿-- Alunos: Marcio Davi, Fábio Villas Boas e Raniere Moura
-- Disciplina: Banco de Dados II
-- Prof: Alexandre Damasceno

-- Database: unirn
-- DROP DATABASE unirn;
CREATE DATABASE unirn
  WITH OWNER = unirn
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1;


-- Schema: bd2
-- DROP SCHEMA bd2;
CREATE SCHEMA bd2
  AUTHORIZATION unirn;


-- Sequence: bd2.hibernate_sequence
-- DROP SEQUENCE bd2.hibernate_sequence;
CREATE SEQUENCE bd2.hibernate_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 49
  CACHE 1;
ALTER TABLE bd2.hibernate_sequence
  OWNER TO unirn;


-- Table: bd2.disciplina
-- DROP TABLE bd2.disciplina;
CREATE TABLE bd2.disciplina
(
  id bigint NOT NULL,
  version bigint NOT NULL,
  carga_horaria integer NOT NULL,
  codigo character varying(255) NOT NULL,
  nome character varying(255) NOT NULL,
  professor_id bigint,
  CONSTRAINT disciplina_pkey PRIMARY KEY (id),
  CONSTRAINT fk_n27x6vngvsn9jemjwmtibimx5 FOREIGN KEY (professor_id)
      REFERENCES bd2.pessoa (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uk_eljvghu5wa2lowt86mlf41ldq UNIQUE (codigo)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE bd2.disciplina
  OWNER TO unirn;


-- Table: bd2.nota
-- DROP TABLE bd2.nota;
CREATE TABLE bd2.nota
(
  id bigint NOT NULL,
  version bigint NOT NULL,
  aluno_id bigint NOT NULL,
  date_created timestamp without time zone NOT NULL,
  disciplina_id bigint NOT NULL,
  last_updated timestamp without time zone NOT NULL,
  valor real NOT NULL,
  CONSTRAINT nota_pkey PRIMARY KEY (id),
  CONSTRAINT fk_ibe4htnd5d6fe90u2axnjwklt FOREIGN KEY (disciplina_id)
      REFERENCES bd2.disciplina (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_jabtbrsx5cmtmxmvonfvgb558 FOREIGN KEY (aluno_id)
      REFERENCES bd2.pessoa (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE bd2.nota
  OWNER TO unirn;


-- Table: bd2.pessoa
-- DROP TABLE bd2.pessoa;
CREATE TABLE bd2.pessoa
(
  id bigint NOT NULL,
  version bigint NOT NULL,
  email character varying(255),
  login character varying(255) NOT NULL,
  matricula character varying(255),
  nome character varying(255),
  senha_hash character varying(255) NOT NULL,
  tipo_pessoa character varying(255) NOT NULL,
  CONSTRAINT pessoa_pkey PRIMARY KEY (id),
  CONSTRAINT uk_csab60w9yycjb127hsdv7wqf5 UNIQUE (login),
  CONSTRAINT uk_fthvu7gx1dgqwixgfy4qlbfvr UNIQUE (matricula)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE bd2.pessoa
  OWNER TO unirn;
