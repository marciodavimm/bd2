package br.unirn.bd2.acesso

class AutenticacaoController {

    def index() { }

    def login(){
        def login = params?.login
        String senha = params?.senha
        def usuario = Pessoa.findByLoginAndSenhaHash( login, senha?.encodeAsSHA1() )
        if(usuario) {
            session.usuario = usuario
            redirect controller: 'index', action: 'index'
            return
        }
        else{
            flash.message = "login/senha incorreto(s)..."
            redirect action: 'index'
            return
        }
    }

    def logout(){
        if(session.usuario) {
            session.invalidate()
        }
        redirect action: 'index'
    }
}
