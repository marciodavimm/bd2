import br.unirn.bd2.acesso.Pessoa
import br.unirn.bd2.base.TipoPessoa

class BootStrap {

    def init = {
        def pessoaLogin = Pessoa.findByLoginAndSenhaHash("unirn", "unirn".encodeAsSHA1())?: new Pessoa( login: "unirn", senha: "unirn", nome: "Usuário de Teste UNIRN", email: "unirnteste@teste.teste", tipoPessoa: TipoPessoa.PROFESSOR).save(failOnError: true)

    }
    def destroy = {
    }
}
