package br.unirn.bd2.acesso

import br.unirn.bd2.base.TipoPessoa

class SegurancaFilters {

    def filters = {
        all(controller:'*', action:'*') {
            before = {
                if(!session?.usuario && !"autenticacao".equals(controllerName) && !"assets".equals(controllerName) ){
                    redirect(controller:"autenticacao",action:"index")
                    return false
                }
            }
            after = { Map model ->

            }
            afterView = { Exception e ->

            }
        }

        aluno(controller:"*", action:"*create*|*edit*|*update*|*save*|*delete*"){
            before = {
                if (session?.usuario?.tipoPessoa == TipoPessoa.ALUNO) {
                    flash.message = "${session?.usuario} não tem permissão para a funcionalidade desejada ('${actionName}' em '${controllerName}')."
                    if("delete".equals(actionName)) {
                        render view: '/index/index'
                        return false
                    }
                    redirect(controller: 'index', action: 'index')
                    return false
                }
            }
        }
    }
}
