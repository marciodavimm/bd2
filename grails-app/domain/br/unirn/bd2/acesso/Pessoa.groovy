package br.unirn.bd2.acesso

import br.unirn.bd2.base.TipoPessoa

class Pessoa {

    String login
    String senha
    String senhaHash

    String matricula
    String nome
    TipoPessoa tipoPessoa
    String email

    static transients = ['senha']

    static constraints = {
        login nullable: false, blank: false, unique: true
        senha nullable: false, blank: false
        tipoPessoa nullable: false
        matricula nullable: true, unique: true
        nome nullable: true
        email nullable: true
    }

    def setSenha(String pass){
        if(pass){
            senha = pass
            senhaHash = senha.encodeAsSHA1()
        }
    }

    @Override
    String toString(){
        "${nome?:login} (${tipoPessoa})"
    }
}
