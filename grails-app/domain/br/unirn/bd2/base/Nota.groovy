package br.unirn.bd2.base

import br.unirn.bd2.acesso.Pessoa

class Nota {

    float valor
    Pessoa aluno
    Disciplina disciplina

    Date dateCreated
    Date lastUpdated


    static constraints = {
        valor(min: 0F, max: 10F)
        aluno(nullable: false, validator: { val ->
            if( val?.tipoPessoa != TipoPessoa.ALUNO) return ["tipoErrado"]
        })
        disciplina(nullable: false)
    }
}
