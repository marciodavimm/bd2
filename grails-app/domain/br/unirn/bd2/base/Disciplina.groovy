package br.unirn.bd2.base

import br.unirn.bd2.acesso.Pessoa
import br.unirn.bd2.base.TipoPessoa

class Disciplina {

    String codigo
    String nome
    int cargaHoraria
    Pessoa professor

    static constraints = {
        codigo (unique: true, nullable: false)
        nome(nullable: false )
        cargaHoraria(min: 0, max: 360)
        professor ( nullable: true, validator: { val ->
            if(!val) return true
            if( val?.tipoPessoa != TipoPessoa.PROFESSOR ) return ['tipoErrado']
        })
    }

    @Override
    String toString(){
        "${codigo?:"S/COD"} - ${nome?:"S/NOME"}"
    }
}
