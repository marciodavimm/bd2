<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Entrar no Sistema</title>
</head>
<body>
<div class="account-container stacked">

    <div class="content clearfix">

        <br/>
    <div class="col-lg-offset-4 col-lg-4">

    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <h3 class="panel-title"><strong>Autenticação</strong></h3>
        </div>
    <div class="panel-body">

        <g:form controller="autenticacao" action="login">
            <g:if test="${flash.message}">
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                    <strong>${flash.message}</strong>
                </div>
            </g:if>
            <g:if test="${flash.error}">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                    <strong>${flash.error}</strong>
                </div>
            </g:if>

            <p>Informe uma conta registrada:</p>

            <div >
                <label for="login">Login:</label>
                <g:textField name="login" class="form-control input-lg username-field" placeholder="login" value="${grails.util.Environment.developmentMode?'unirn':''}"/>
            </div> <!-- /field -->
            <br/>
            <div>
                <label for="password">Senha:</label>
                <g:passwordField name="senha" class="form-control input-lg password-field" placeholder="Senha" value="${grails.util.Environment.developmentMode?'unirn':''}"/>
            </div> <!-- /password -->

            <div class="login-actions">
                <br/>
                <button class="login-action btn btn-primary pull-right">Entrar</button>
            </div> <!-- .actions -->

                    </div>
                </div>
            </div>

        </g:form>

    </div> <!-- /content -->

</div> <!-- /account-container -->
</body>
</html>
