<%@ page import="org.codehaus.groovy.grails.io.support.GrailsResourceUtils; org.codehaus.groovy.grails.commons.spring.GrailsApplicationContext; grails.util.GrailsUtil" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">

    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>
    <g:layoutHead/>

    <style type="text/css">

    #footer {
        font-size: 0.75em;
        font-style: italic;
        padding: 2em 1em 2em 1em;
        margin-bottom: 1em;
        margin-top: 1em;
        clear: both;
    }

    /*requerido para usar navbar com navbar-fixed-top */
    body { padding-top: 50px; }
    </style>
</head>
<body>

<g:render template="/layouts/myheader" />

<br/>
<div class="row">
    <div class="col-lg-offset-1 col-lg-10">
        <div class="alert alert-info">
            <g:layoutBody/>

            %{--
            <div id="footer" class="footer text-center" role="contentinfo">
                <g:render template="/layouts/myfooter" />
            </div>
            --}%
        </div>
    </div>

    <g:javascript>
        $("[data-toggle=tooltip]").tooltip();
    </g:javascript>
</div>

</body>
</html>
