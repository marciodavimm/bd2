
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                %{--<span class="icon-bar"></span>--}%
                %{--<span class="icon-bar"></span>--}%
                %{--<span class="icon-bar"></span>--}%
            </button>
            <g:link class="navbar-brand" controller="index" data-toggle="tooltip" data-placement="bottom" title="Home">CadNotas</g:link>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><g:link url="[action:'info', controller:'index']" data-toggle="tooltip" data-placement="bottom" title="Informações sobre a Aplicação, Cadastros Auxiliares" >Informações</g:link></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Opções<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li class="divider"></li>

                    <li class="dropdown-submenu"> <a tabindex="-1" href="#">Styled Marker</a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="principal" action="styledMarkerEx">SM Examples</g:link></li>
                            <li><g:link controller="principal" action="styledMarkerRef">SM Reference</g:link></li>
                            <li class="dropdown-submenu"> <a href="#">More..</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Level 3</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown-submenu"> <a tabindex="-1" href="#">Marker Clusterer Plus</a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="principal" action="markerClustererEx">MCP Examples</g:link></li>
                            <li><g:link controller="principal" action="markerClustererRef">MCP Reference</g:link></li>
                            <li class="dropdown-submenu"> <a href="#">More..</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Level 3</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>

        <ul class="nav navbar-nav navbar-right">
           %{-- <g:if test="${!session?.usuario}">
                <g:form controller="autenticacao" action="login" method="post"
                        class="navbar-form" role="form">
                    <div class="form-group">
                        <input type="text" name="login" placeholder="Login" class="form-control input-sm">
                    </div>
                    <div class="form-group">
                        <input type="password" name="senha" placeholder="Senha" class="form-control input-sm">
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Entrar</button>
                </g:form>
            </g:if>

            <g:else>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">${session?.usuario} <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><g:link controller="pessoa" action="show" id="${session?.usuario?.id}">Detalhes</g:link></li>
                        <li class="divider"></li>
                        <li><g:link controller="autenticacao" action="logout">Sair</g:link></li>
                    </ul>
                </li>
            </g:else>--}%

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">${session?.usuario} <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><g:link controller="pessoa" action="show" id="${session?.usuario?.id}">Detalhes</g:link></li>

                    <g:if test="${session?.usuario?.tipoPessoa==br.unirn.bd2.base.TipoPessoa.ALUNO}">
                        <li><g:link controller="pessoa" action="minhasNotas" resource="${session?.usuario}" id="${session?.usuario?.id}">Minhas Notas</g:link></li>
                    </g:if>
                    <g:if test="${session?.usuario?.tipoPessoa==br.unirn.bd2.base.TipoPessoa.PROFESSOR}">
                        <li><g:link controller="pessoa" action="minhasDisciplinas" resource="${session?.usuario}" id="${session?.usuario?.id}">Minhas Disciplinas</g:link></li>
                    </g:if>

                    <li class="divider"></li>
                    <li><g:link controller="autenticacao" action="logout">Sair</g:link></li>
                </ul>
            </li>
        </ul>

    </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

