<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main"/>
	<title>Welcome to Grails</title>
</head>
<body>

	<g:if test="${flash.message}">
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"><span
					aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
			<strong>${flash.message}</strong>
		</div>
	</g:if>
	<g:if test="${flash.error}">
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"><span
					aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
			<strong>${flash.error}</strong>
		</div>
	</g:if>

	<h1>Controle de Notas</h1>

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#pessoaId" aria-expanded="true" aria-controls="collapseOne">
						Pessoas/Usuários
					</a>
				</h4>
			</div>
			<div id="pessoaId" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				%{--<div class="panel-body">--}%
				<ul class="list-group">
					<li class="list-group-item">
						<p>Executa funções primárias relacionadas a Pessoas: cadastro, edição, listagem e remoção de pessoas. </p>
						<p>Uma pessoa pode ser do tipo ALUNO ou PROFESSOR. Alunos podem apenas visualizar dados, enquanto professores podem além disso, cadastrar, editar e excluir dados.</p>
					</li>
					<li class="list-group-item"><g:link class="btn btn-primary btn-sm" controller="pessoa">Ir Para Controlador Pessoa</g:link></li>
				</ul>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTwo">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#notaId" aria-expanded="true" aria-controls="collapseTwo">
						Notas
					</a>
				</h4>
			</div>
			<div id="notaId" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				%{--<div class="panel-body">--}%
				<ul class="list-group">
					<li class="list-group-item">
						<p>Executa funções primárias relacionadas a Notas:cadastro, edição, listagem e remoção de entradas.</p>
						<p>Cada Nota possui uma pessoa e disciplina associada. A pessoa associada a uma nota deve ser do tipo ALUNO.</p>
					</li>
					<li class="list-group-item"><g:link class="btn btn-primary btn-sm" controller="nota">Ir Para Controlador Nota</g:link></li>
				</ul>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingThree">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#disciplinaId" aria-expanded="true" aria-controls="collapseThree">
						Disciplinas
					</a>
				</h4>
			</div>
			<div id="disciplinaId" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				%{--<div class="panel-body">--}%
				<ul class="list-group">
					<li class="list-group-item">
						<p>Executa funções primárias relacionadas a Disciplinas: cadastro, edição, listagem e remoção de entradas. </p>
						<p>Cada disciplina pode ter uma pessoa (professor) associada. A pessoa deve ser do tipo PROFESSOR.</p>
					</li>
					<li class="list-group-item"><g:link class="btn btn-primary btn-sm" controller="disciplina">Ir Para Controlador Disciplina</g:link></li>
				</ul>
			</div>
		</div>



		%{--<g:each status="num" var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading${num?.toString()?.toUpperCase()}">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#${c.shortName}" aria-expanded="true" aria-controls="collapseOne">
							${c.naturalName}
						</a>
					</h4>
				</div>
				<div id="${c.shortName}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading${num?.toString()?.toUpperCase()}">
					--}%%{--<div class="panel-body">--}%%{--
					<ul class="list-group">
						<li class="list-group-item">
							<p>Executa funções primárias relacionadas a ${c.name}, incluindo cadastro, edição, listagem e remoção de entradas.</p>
						</li>
						<li class="list-group-item"><g:link class="btn btn-primary btn-sm" controller="${c.logicalPropertyName}">Ir Para ${c.name}</g:link></li>
					</ul>
				</div>
			</div>

		</g:each>--}%

	</div>

<g:javascript>
	console.log("usuario ? = ${session?.usuario}");
</g:javascript>

</body>
</html>
