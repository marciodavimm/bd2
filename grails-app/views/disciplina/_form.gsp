<%@ page import="br.unirn.bd2.base.Disciplina" %>



<div class="fieldcontain ${hasErrors(bean: disciplinaInstance, field: 'codigo', 'error')} required">
	<label for="codigo">
		<g:message code="disciplina.codigo.label" default="Codigo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="codigo" required="" value="${disciplinaInstance?.codigo}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: disciplinaInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="disciplina.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${disciplinaInstance?.nome}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: disciplinaInstance, field: 'cargaHoraria', 'error')} required">
	<label for="cargaHoraria">
		<g:message code="disciplina.cargaHoraria.label" default="Carga Horaria" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="cargaHoraria" type="number" min="0" max="360" value="${disciplinaInstance.cargaHoraria}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: disciplinaInstance, field: 'professor', 'error')}">
	<label for="professor">
		<g:message code="disciplina.professor.label" default="Professor" />
	</label>
	<g:select id="professor" noSelection="['':'--Sem Professor--']" name="professor.id" from="${br.unirn.bd2.acesso.Pessoa.list()}" optionKey="id" value="${disciplinaInstance?.professor?.id}" class="many-to-one"/>

</div>

