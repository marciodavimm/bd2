
<%@ page import="br.unirn.bd2.base.Disciplina" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'disciplina.label', default: 'Disciplina')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<div class="nav" role="navigation">
	<ul>
		<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
		<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
	</ul>
</div>
<div id="list-disciplina" class="content scaffold-list" role="main">
	<h1><g:message code="default.list.label" args="[entityName]" /></h1>
	<g:if test="${flash.message}">
		<div class="message" role="status">${flash.message}</div>
	</g:if>
	<table class="table table-bordered table-condensed table-hover">
		<thead>
		<tr class="warning">

			<g:sortableColumn property="codigo" title="${message(code: 'disciplina.codigo.label', default: 'Codigo')}" />

			<g:sortableColumn property="nome" title="${message(code: 'disciplina.nome.label', default: 'Nome')}" />

			<g:sortableColumn property="cargaHoraria" title="${message(code: 'disciplina.cargaHoraria.label', default: 'Carga Horaria')}" />

			<th><g:message code="disciplina.professor.label" default="Professor" /></th>

		</tr>
		</thead>
		<tbody>
		<g:each in="${disciplinaInstanceList}" status="i" var="disciplinaInstance">
			<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

				<td><g:link action="show" id="${disciplinaInstance.id}">${fieldValue(bean: disciplinaInstance, field: "codigo")}</g:link></td>

				<td>${fieldValue(bean: disciplinaInstance, field: "nome")}</td>

				<td>${fieldValue(bean: disciplinaInstance, field: "cargaHoraria")}</td>

				<td>${fieldValue(bean: disciplinaInstance, field: "professor")}</td>

			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<g:paginate total="${disciplinaInstanceCount ?: 0}" />
	</div>
</div>
</body>
</html>
