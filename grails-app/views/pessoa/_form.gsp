<%@ page import="br.unirn.bd2.acesso.Pessoa" %>


<div class="fieldcontain ${hasErrors(bean: pessoaInstance, field: 'login', 'error')} required">
	<label for="login">
		<g:message code="pessoa.login.label" default="Login" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="login" required="" value="${pessoaInstance?.login}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: pessoaInstance, field: 'senha', 'error')} required">
	<label for="senha">
		<g:message code="pessoa.senha.label" default="Senha" />
		<span class="required-indicator">*</span>
	</label>
	<g:passwordField name="senha" required="" value="${pessoaInstance?.senha}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: pessoaInstance, field: 'nome', 'error')}">
	<label for="nome">
		<g:message code="pessoa.nome.label" default="Nome" />
	</label>
	<g:textField name="nome" required="" value="${pessoaInstance?.nome}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: pessoaInstance, field: 'matricula', 'error')}">
	<label for="matricula">
		<g:message code="pessoa.matricula.label" default="Matrícula" />
	</label>
	<g:textField name="matricula" value="${pessoaInstance?.matricula}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: pessoaInstance, field: 'email', 'error')}">
	<label for="email">
		<g:message code="pessoa.email.label" default="Email" />
	</label>
	<g:field type="email" name="email" value="${pessoaInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: pessoaInstance, field: 'tipoPessoa', 'error')} required">
	<label for="tipoPessoa">
		<g:message code="pessoa.tipoPessoa.label" default="Tipo Pessoa" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="tipoPessoa" from="${br.unirn.bd2.base.TipoPessoa?.values()}" keys="${br.unirn.bd2.base.TipoPessoa.values()*.name()}" required="" value="${pessoaInstance?.tipoPessoa?.name()}" />
</div>

