
<%@ page import="br.unirn.bd2.base.Nota" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'nota.label', default: 'Nota')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-nota" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
	<ul>
		<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
		<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
	</ul>
</div>
<div id="list-nota" class="content scaffold-list" role="main">
	<h1><g:message code="default.list.label" args="[entityName]" /></h1>
	<g:if test="${flash.message}">
		<div class="message" role="status">${flash.message}</div>
	</g:if>
	<table class="table table-bordered table-condensed table-hover">
		<thead>
		<tr class="warning">

			<g:sortableColumn property="valor" title="${message(code: 'nota.valor.label', default: 'Valor')}" />

			<th><g:message code="nota.aluno.label" default="Aluno" /></th>

			<th><g:message code="nota.disciplina.label" default="Disciplina" /></th>

			<g:sortableColumn property="dateCreated" title="${message(code: 'nota.dateCreated.label', default: 'Date Created')}" />

			<g:sortableColumn property="lastUpdated" title="${message(code: 'nota.lastUpdated.label', default: 'Last Updated')}" />

		</tr>
		</thead>
		<tbody>
		<g:each in="${notaInstanceList}" status="i" var="notaInstance">
			<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

				<td><g:link action="show" id="${notaInstance.id}">${fieldValue(bean: notaInstance, field: "valor")}</g:link></td>

				<td>${fieldValue(bean: notaInstance, field: "aluno")}</td>

				<td>${fieldValue(bean: notaInstance, field: "disciplina")}</td>

				<td><g:formatDate date="${notaInstance.dateCreated}" /></td>

				<td><g:formatDate date="${notaInstance.lastUpdated}" /></td>

			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<g:paginate total="${notaInstanceCount ?: 0}" />
	</div>
</div>
</body>
</html>
