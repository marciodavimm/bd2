
<%@ page import="br.unirn.bd2.acesso.Pessoa" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pessoa.label', default: 'Pessoa')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>

	<body>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-pessoa" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>


			<table class="table table-bordered table-condensed table-hover">
				<thead>
				<tr class="warning">

					<g:sortableColumn property="login" title="${message(code: 'pessoa.login.label', default: 'Login')}" />

					<g:sortableColumn property="nome" title="${message(code: 'pessoa.nome.label', default: 'Nome')}" />

					<g:sortableColumn property="tipoPessoa" title="${message(code: 'pessoa.tipoPessoa.label', default: 'Tipo Pessoa')}" />

					<g:sortableColumn property="email" title="${message(code: 'pessoa.email.label', default: 'Email')}" />

					<g:sortableColumn property="senhaHash" title="${message(code: 'pessoa.senhaHash.label', default: 'Senha Hash')}" />

				</tr>
				</thead>
				<tbody>
				<g:each in="${pessoaInstanceList}" status="i" var="pessoaInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td><g:link action="show" id="${pessoaInstance.id}">${fieldValue(bean: pessoaInstance, field: "login")}</g:link></td>

						<td>${fieldValue(bean: pessoaInstance, field: "nome")}</td>

						<td>${fieldValue(bean: pessoaInstance, field: "tipoPessoa")}</td>

						<td>${fieldValue(bean: pessoaInstance, field: "email")}</td>

						<td>${fieldValue(bean: pessoaInstance, field: "senhaHash")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${pessoaInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
