<%@ page import="br.unirn.bd2.base.Nota" %>



<div class="fieldcontain ${hasErrors(bean: notaInstance, field: 'valor', 'error')} required">
	<label for="valor">
		<g:message code="nota.valor.label" default="Valor" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="valor" value="${fieldValue(bean: notaInstance, field: 'valor')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: notaInstance, field: 'aluno', 'error')} required">
	<label for="aluno">
		<g:message code="nota.aluno.label" default="Aluno" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="aluno" name="aluno.id" noSelection="['':'--Escolha--']" from="${br.unirn.bd2.acesso.Pessoa.list()}" optionKey="id" required="" value="${notaInstance?.aluno?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: notaInstance, field: 'disciplina', 'error')} required">
	<label for="disciplina">
		<g:message code="nota.disciplina.label" default="Disciplina" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="disciplina" name="disciplina.id" from="${br.unirn.bd2.base.Disciplina.list()}" optionKey="id" required="" value="${notaInstance?.disciplina?.id}" class="many-to-one"/>

</div>

